-module(totientrange).
-export([hcf/2,
	relprime/2,
	euler/1,
	sumTotient/2,
	start/0,
	server/0,
	worker/0,
	watcher/3,
	testRobust/2
]).

%% TotientRange.erl - Sequential Euler Totient Function (Erlang Version)
%% compile from the shell: >c(totientrange).
%% run from the shell:     >totientrange:sumTotient(1,1000).

%% Phil Trinder 20/10/2018

%% This program calculates the sum of the totients between a lower and an
%% upper limit. It is based on earlier work by: Nathan Charles,
%% Hans-Wolfgang Loidl and Colin Runciman

%% The comments provide (executable) Haskell specifications of the functions

%% hcf x 0 = x
%% hcf x y = hcf y (rem x y)

hcf(X,0) -> X;
hcf(X,Y) -> hcf(Y,X rem Y).

%% relprime x y = hcf x y == 1

relprime(X,Y) ->
	V = hcf(X,Y),
	if
		V == 1 -> true;
		true -> false
	end.

%%euler n = length (filter (relprime n) (mkList n))

euler(N) ->
	RelprimeN = fun(Y) -> relprime(N,Y) end,
	length (lists:filter(RelprimeN,(lists:seq(1,N)))).

%% Take completion timestamp, and print elapsed time

printElapsed(S,US) ->
	{_, S2, US2} = os:timestamp(),
	%% Adjust Seconds if completion Microsecs > start Microsecs
	if
		US2-US < 0 ->
			S3 = S2-1,
			US3 = US2+1000000;
		true ->
			S3 = S2,
			US3 = US2
	end,
	io:format("Time taken in Secs, MicroSecs ~p ~p~n",[S3-S,US3-US]).


%%sumTotient lower upper = sum (map euler [lower, lower+1 .. upper])

sumTotient(Lower,Upper) ->
	lists:sum(lists:map(fun euler/1,lists:seq(Lower, Upper))).

start() ->
	register(server, spawn(totientrange, server, [])).

server() ->
	receive
		finished -> io:fwrite("Shutting down.\n");
		{range, Lower, Upper, NWorkers} ->
			{_, S, US} = os:timestamp(),
			server_createWorkers(NWorkers),
			server_createWatchers(Lower, Upper, NWorkers),
			server_sendRanges(Lower, Upper, NWorkers),
			io:fwrite("Server: sum of totients: ~p\n", [server_sumResponses(NWorkers)]),
			server_sendFinisheds(NWorkers),
			printElapsed(S,US),
			server()
	end.

server_createWorkers(Countdown) ->
	if
		Countdown == 0 -> ok;
		true ->
			register(workerName(Countdown), spawn(totientrange, worker, [])),
			server_createWorkers(Countdown - 1)
	end.

server_createWatchers(Lower, Upper, Countdown) ->
	if
		Countdown == 0 -> ok;
		true ->
			io:fwrite("Watcher: watching ~p\n", [workerName(Countdown)]),
			Intermediate = Lower + ((Upper-Lower) div Countdown),
			register(watcherName(Countdown), spawn(totientrange, watcher, [Lower, Intermediate, Countdown])),
			server_createWatchers(Intermediate+1, Upper, Countdown - 1)
	end.

server_sendRanges(Lower, Upper, Countdown) ->
	if
		Countdown == 0 -> ok;
		true ->
			Intermediate = Lower + ((Upper-Lower) div Countdown),
			workerName(Countdown) ! {range, Lower, Intermediate},
			server_sendRanges(Intermediate+1, Upper, Countdown - 1)
	end.

server_sumResponses(Countdown) ->
	if
		Countdown == 0 -> 0;
		true ->
			receive
				{result, Result} ->
					io:fwrite("Server: receieved sum ~p\n", [Result]),
					Result + server_sumResponses(Countdown - 1)
			end
	end.

server_sendFinisheds(Countdown) ->
	if
		Countdown == 0 -> ok;
		true ->
			watcherName(Countdown) ! finished,
			server_sendFinisheds(Countdown - 1)
	end.

worker() ->
	receive
		{range, Lower, Upper} ->
			io:fwrite("Worker: computing range (~p, ~p).\n", [Lower, Upper]),
			Result = sumTotient(Lower, Upper),
			server ! {result, Result}
	end.

watcher(Lower, Upper, Num) ->
	process_flag(trap_exit, true),
	WorkerPid = whereis(workerName(Num)),
	if
		WorkerPid =/= undefined ->
			link(WorkerPid);
		true ->
			ok
	end,
	receive
		finished -> ok;
		{'EXIT',_,Reason} ->
			if
				Reason == normal -> watcher(Lower, Upper, Num);
				true ->
					register(workerName(Num), spawn(totientrange, worker, [])),
					workerName(Num) ! {range, Lower, Upper},
					watcher(Lower, Upper, Num)
			end
	end.

workerName(Num) ->
	list_to_atom( "worker" ++ integer_to_list( Num )).

watcherName(Num) ->
	list_to_atom( "watcher" ++ integer_to_list( Num )).

workerChaos(NVictims,NWorkers) ->
	lists:map(
		fun( _ ) ->
			timer:sleep(500),

			WorkerNum = rand:uniform(NWorkers),
			io:format("workerChaos killing ~p~n", [workerName(WorkerNum)]),

			WorkerPid = whereis(workerName(WorkerNum)),
			if
				WorkerPid == undefined ->
					io:format("workerChaos already dead: ~p~n",[workerName(WorkerNum)]);
				true ->
					exit(whereis(workerName(WorkerNum)), chaos)
			end
		end,
		lists:seq(1, NVictims)
	).

testRobust(NWorkers,NVictims) ->
	ServerPid = whereis(server),
	if
		ServerPid == undefined ->
			start();
		true ->
			ok
	end,
	server ! {range, 1, 15000, NWorkers},
	workerChaos(NVictims,NWorkers).
