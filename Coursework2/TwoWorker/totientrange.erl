-module(totientrange).
-export([hcf/2,
	relprime/2,
	euler/1,
	sumTotient/2,
	start/0,
	server/0,
	worker/0
]).

%% TotientRange.erl - Sequential Euler Totient Function (Erlang Version)
%% compile from the shell: >c(totientrange).
%% run from the shell:     >totientrange:sumTotient(1,1000).

%% Phil Trinder 20/10/2018

%% This program calculates the sum of the totients between a lower and an
%% upper limit. It is based on earlier work by: Nathan Charles,
%% Hans-Wolfgang Loidl and Colin Runciman

%% The comments provide (executable) Haskell specifications of the functions

%% hcf x 0 = x
%% hcf x y = hcf y (rem x y)

hcf(X,0) -> X;
hcf(X,Y) -> hcf(Y,X rem Y).

%% relprime x y = hcf x y == 1

relprime(X,Y) ->
	V = hcf(X,Y),
	if
		V == 1 -> true;
		true -> false
	end.

%%euler n = length (filter (relprime n) (mkList n))

euler(N) ->
	RelprimeN = fun(Y) -> relprime(N,Y) end,
	length (lists:filter(RelprimeN,(lists:seq(1,N)))).

%% Take completion timestamp, and print elapsed time

printElapsed(S,US) ->
	{_, S2, US2} = os:timestamp(),
	%% Adjust Seconds if completion Microsecs > start Microsecs
	if
		US2-US < 0 ->
			S3 = S2-1,
			US3 = US2+1000000;
		true ->
			S3 = S2,
			US3 = US2
	end,
	io:format("Time taken in Secs, MicroSecs ~p ~p~n",[S3-S,US3-US]).


%%sumTotient lower upper = sum (map euler [lower, lower+1 .. upper])

sumTotient(Lower,Upper) ->
	lists:sum(lists:map(fun euler/1,lists:seq(Lower, Upper))).

start() ->
	register(server, spawn(totientrange, server, [])).

server() ->
	receive
		finished -> io:fwrite("Shutting down.\n");
		{range, Lower, Upper} ->
			{_, S, US} = os:timestamp(),
			register(workerName(1), spawn(totientrange, worker, [])),
			register(workerName(2), spawn(totientrange, worker, [])),
			Mid = (Lower+Upper) div 2,
			worker1 ! {range, Lower, Mid},
			worker2 ! {range, Mid + 1, Upper},
			io:fwrite("Server: sum of totients: ~p\n", [server_sumResponses(2)]),
			worker1 ! finished,
			worker2 ! finished,
			printElapsed(S,US),
			server()
	end.

server_sumResponses(Countdown) ->
	if
		Countdown == 0 -> 0;
		true ->
			receive
				{result, Result} ->
					io:fwrite("Server: receieved sum ~p\n", [Result]),
					Result + server_sumResponses(Countdown - 1)
			end
	end.

worker() ->
	receive
		finished -> io:fwrite("Worker: finished.\n");
		{range, Lower, Upper} ->
			io:fwrite("Worker: computing range (~p, ~p).\n", [Lower, Upper]),
			Result = sumTotient(Lower, Upper),
			server ! {result, Result},
			worker()
	end.

workerName(Num) ->
	list_to_atom( "worker" ++ integer_to_list( Num )).
