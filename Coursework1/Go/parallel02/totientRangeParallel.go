package main

import (
  "fmt"
  "os"
  "strconv"
  "time"
)


// Compute the Highest Common Factor, hcf of two numbers x and y
//
// hcf x 0 = x
// hcf x y = hcf y (rem x y)

func hcf(x, y int64) int64 {
  var t int64
  for (y != 0) {
    t = x%y
    x = y
    y = t
  }
  return x
}

// relprime determines whether two numbers x and y are relatively prime
//
// relprime x y = hcf x y == 1

func relprime(x,y int64) bool {
  return hcf(x, y) == 1;
}

// euler(n) computes the Euler totient function, i.e. counts the number of
// positive integers up to n that are relatively prime to n
//
// euler n = length (filter (relprime n) [1 .. n-1])

func euler(n int64) int64 {
  var length, i int64

  length = 0
  for i = 1; i < n; i++ {
    if relprime(n, i) {length++}
  }
  return length
}

// sumTotient lower upper sums the Euler totient values for all numbers
// between "lower" and "upper".
//
// sumTotient lower upper = sum (map euler [lower, lower+1 .. upper])

func sumTotient(lower,upper,numThreads int64, out chan int64) {
  var i,sum int64

  if numThreads == 1 {
    sum = 0
    for i = lower; i <= upper; i++ {
      sum += euler(i)
    }
    out <- sum
  } else {
    result1 := make(chan int64)
    result2 := make(chan int64)

    go sumTotient(lower, (upper+lower)/2, numThreads/2, result1)
    go sumTotient(((upper+lower)/2)+1, upper, numThreads/2, result2)

    out <- (<-result1) + (<-result2)
  }
}

func main() {
  var lower, upper, numThreads int64
  var err error
                                             // Read and validate lower and upper arguments
  if len(os.Args) < 4 {
    panic(fmt.Sprintf("Usage: must provide lower and upper range limits as arguments"))
  }

  if lower, err = strconv.ParseInt(os.Args[1],10,64) ; err != nil {
    panic(fmt.Sprintf("Can't parse first argument"))
  }
  if upper, err = strconv.ParseInt(os.Args[2],10,64) ; err != nil {
    panic(fmt.Sprintf("Can't parse second argument"))
  }
  if numThreads, err = strconv.ParseInt(os.Args[3],10,64) ; err != nil {
    panic(fmt.Sprintf("Can't parse third argument"))
  }

                                             // Record start time
  start := time.Now()

  result := make(chan int64)
  go sumTotient(lower, upper, numThreads, result)
                                             // Compute and output sum of totients
  fmt.Println("Sum of Totients between", lower, "and", upper, "is", (<-result))
                                             // Record the elapsed time
  t := time.Now()
  elapsed := t.Sub(start)
  fmt.Println("Elapsed time", elapsed)
}
