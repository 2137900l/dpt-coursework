#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <omp.h>

// hcf x 0 = x
// hcf x y = hcf y (rem x y)

long hcf(long x, long y)
{
  long t;

  while (y != 0) {
    t = x % y;
    x = y;
    y = t;
  }
  return x;
}


// relprime x y = hcf x y == 1

int relprime(long x, long y)
{
  return hcf(x, y) == 1;
}


// euler n = length (filter (relprime n) [1 .. n-1])

long euler(long n)
{
  long length, i;

  length = 0;
  for (i = 1; i < n; i++)
    if (relprime(n, i))
      length++;
  return length;
}


// sumTotient lower upper = sum (map euler [lower, lower+1 .. upper])

long sumTotientWorker(long lower, long upper)
{
  long sum, i;

  sum = 0;
  for (i = lower; i <= upper; i++)
    sum = sum + euler(i);
  return sum;
}

long sumTotientMaster(long lower, long upper, int num_threads)
{
  long sum, subtask_size;
  long* results;

  results = (long*)malloc(sizeof(long) * num_threads);
  subtask_size = (upper-lower)/num_threads;

  #pragma omp parallel shared(results)
  {
    int thread_num = omp_get_thread_num();
    long subtask_lower = lower + (thread_num * subtask_size) + 1;
    long subtask_upper = lower + ((thread_num+1) * subtask_size);

    if(thread_num == 0){ // first batch
      results[thread_num] = sumTotientWorker(lower, subtask_upper);
    }
    else if(thread_num == num_threads-1){ // last batch
      results[thread_num] = sumTotientWorker(subtask_lower, upper);
    }
    else{ // middle batches
      results[thread_num] = sumTotientWorker(subtask_lower, subtask_upper);
    }
  }

  sum = 0;
  for(int i = 0; i < num_threads; i++){
    sum += results[i];
  }

  return sum;
}


int main(int argc, char ** argv)
{
  long lower, upper;
  int num_threads;

  if (argc != 4) {
    printf("not 3 arguments\n");
    return 1;
  }
  sscanf(argv[1], "%ld", &lower);
  sscanf(argv[2], "%ld", &upper);
  sscanf(argv[3], "%d", &num_threads);

  omp_set_num_threads(num_threads);

  struct timeval start, end;

  gettimeofday(&start, NULL);

  printf("C: Sum of Totients  between [%ld..%ld] is %ld\n",
         lower, upper, sumTotientMaster(lower, upper, num_threads));

  gettimeofday(&end, NULL);
                                                                                                                                                                                                                                                      //printf("Time elapsed: %f seconds\n", ((double)(end-start))/CLOCKS_PER_SEC);
  printf("Time elapsed: %f seconds\n", (double)(end.tv_usec-start.tv_usec) / 1000000 + (double)(end.tv_sec-start.tv_sec));

  return 0;
}
